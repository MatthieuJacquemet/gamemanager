import os

class GameManagerCore:

    def __init__(self):
        self.games = list()
    
    def find_games(self, path):
        "find game witch it's path matches the given path"
        for game in self.games:
            if game.exe.startswith(path + os.sep):
                return game
    
    def find_appid(self, appid):
        "find game whitch it's appid matches the given id"
        for game in self.games:
            if game.appid == appid:
                return game
        return None

    def create_game(self):
        pass
    
    def delete_game(self):
        pass
    
    def save_games(self):
        pass

    def load_games(self):
        pass
