import urllib.request as req
from bs4 import BeautifulSoup, SoupStrainer
from .imageloader import ImageProvider
import json
import time
import re

@ImageProvider.register("GoogleImage")
class GoogleImageScraper(ImageProvider):

    @classmethod
    def get_image(cls, image, user_agent, size_w=None, size_h=None, max_image=10):
        cls.list_urls(image, user_agent, size_w, size_h, max_image)
        # TODO similar images grouping to find most revelent one

    @staticmethod
    def _create_url(search, **params):
        kw = search.replace(" ", "+")
        pm = "&".join("%s=%s"%(k,v) for k,v in params.items())
        return "https://www.google.com/search?q={}&{}".format(kw,pm)

    @classmethod
    def list_urls(cls, image, user_agent, size_w=None, size_h=None, max_image=10):
        if size_w and size_h:
            size = "isz:ex,iszw:%s,iszh:%s"%(size_w,size_h)
            url = GoogleImageScraper._create_url(image, tbm="isch", tbs=size)
        else:
            url = GoogleImageScraper._create_url(image)
        header={'User-Agent':user_agent}
        try:
            html = req.urlopen(req.Request(url, headers=header))
        except Exception as e:
            raise StopIteration
            return
        data = html.read()
        pattern = b"<div class=\"rg_meta(.*?)\">(.*?)</div>"
        for i, match in enumerate(re.finditer(pattern, data)):
            if i == max_image:
                break
            json_data = json.loads(match.group(2))
            img_link, img_type = json_data["ou"], json_data["ity"]
            if not img_type:
                img_type = "jpg"
            yield img_link, img_type


        #     match_end = re.search(b"</div>", html[match_start.start():]).group()

        # soup = BeautifulSoup(html, "html.parser", from_encoding="utf8")
        # data = soup.find_all("div", {"class":"rg_meta notranslate"})
        # for i, img_json in enumerate(data):
        #     if i==max_image: break

        
        

