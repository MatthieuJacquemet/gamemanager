import inspect
import requests
import time
import glob
import os
import sys
import imghdr

from utils import thread, ThreadPool, get_filename
from pysteam import game

from .steamcore import SteamCore

valid_extentions = [i[1:] for i in game.Game.valid_custom_image_extensions()]

class InvalidImage(Exception):
    pass

class ImageProvider(SteamCore):

    __providers = dict()

    def __init__(self, grid_path, providers=[], useragent="",
                size_w=None, size_h=None, extrakw="", maximages=10, requestrate=0):
        self.grid_path = grid_path
        self.providers = list()
        self.useragent = useragent
        self.extra_kw = extrakw
        self.size_w = size_w
        self.size_h = size_h
        self.maximages = maximages
        self.req_rate = requestrate
    
    def load_providers(self, *args):
        found = list()
        for i in args:
            try: found.append(ImageProvider.__providers[i])
            except KeyError: pass
        self.providers = found
        return self.providers

    @classmethod    
    def get_image(cls, image, user_agent, size_w=None, size_h=None, max_image=10):
        pass

    @classmethod
    def list_urls(cls, image, user_agent, size_w=None, size_h=None, max_image=10):
        pass

    def find_grid_image(self, appid):
        path = os.path.join(self.grid_path,appid)
        try: return next(glob.iglob(f"{path}.*"))
        except StopIteration:
            return None

    def sync_images(self):
        for img in os.listdir(self.grid_path):
            if not self.find_appid(get_filename(img)):
                print("removing", os.path.join(self.grid_path,img))
                os.remove(os.path.join(self.grid_path,img))
        
        thread_pool = ThreadPool()
        for game in self.games:
            if not self.find_grid_image(game.appid):
                th = self.get_grid_image(game.name, game.appid)
                thread_pool.join(th)
                if self.req_rate:
                    time.sleep(1/self.req_rate)
        thread_pool.wait_all()

    def delete_grid_image(self, appid):
        pass
        # path = self.find_grid_image(appid):
        # if path: os.remove(path)
    
    @thread
    def get_grid_image(self, name, appid):
        if not self.find_grid_image(appid):
            for provider in self.providers:
                for link, ext in provider.list_urls(f"{name} {self.extra_kw}",
                                                self.useragent, self.size_w,
                                                self.size_h, self.maximages):
                    if self.download_image(link, ext, appid):
                        return True
                    if self.req_rate:
                        time.sleep(1/self.req_rate)
    
    def move_grid_image(self, game, oldpath, newpath):
        old_name = os.path.basename(oldpath)
        old_exe = os.path.join(oldpath, os.path.relpath(game.exe, newpath))
        old_image = self.find_grid_image(game.get_appid(old_exe, old_name))
        if old_image:
            os.remove(old_image)
        return self.get_grid_image(game.name, game.appid)

    @classmethod
    def register(cls, item):
        def factory(req_cls):
            cls.__providers[item] = req_cls
            return req_cls
        if inspect.isclass(item):
            req_cls = item
            item = req_cls.__name__
            return factory(req_cls)
        return factory
    
    @classmethod
    def get_provider(cls, name):
        return cls.__providers.get(name, None)
    
    def download_image(self, url, ext, appid):
        path = os.path.join(self.grid_path, appid)
        def get_ex_info():
            return f"{self.find_appid(appid).name} with url : {url}"
        try:
            data = requests.get(url).content
            if imghdr.what("", data) not in valid_extentions:
                return print(f"image type not valid for game : {get_ex_info()}")
            with open(f"{path}.{ext}", "wb") as handler:
                handler.write(data)
                return True
        except IOError as e:
            print("cannot write file :", e)
        except InvalidImage:
            print("image type not valid for game :", get_ex_info())
        except:
            print("cannot download header image for game :", get_ex_info())